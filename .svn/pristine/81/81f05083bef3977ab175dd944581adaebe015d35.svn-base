/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "Sheet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sheet.findAll", query = "SELECT s FROM Sheet s"),
    @NamedQuery(name = "Sheet.findByDrawingNumber", query = "SELECT s FROM Sheet s WHERE s.drawingNumber = :drawingNumber"),
    @NamedQuery(name = "Sheet.findBySheetNumber", query = "SELECT s FROM Sheet s WHERE s.sheetNumber = :sheetNumber"),
    @NamedQuery(name = "Sheet.findByOriginalIssueDate", query = "SELECT s FROM Sheet s WHERE s.originalIssueDate = :originalIssueDate"),
    @NamedQuery(name = "Sheet.findByCurrentRevisionDate", query = "SELECT s FROM Sheet s WHERE s.currentRevisionDate = :currentRevisionDate"),
    @NamedQuery(name = "Sheet.findByCurrentRevisionLetter", query = "SELECT s FROM Sheet s WHERE s.currentRevisionLetter = :currentRevisionLetter"),
    @NamedQuery(name = "Sheet.findByIssueStatus", query = "SELECT s FROM Sheet s WHERE s.issueStatus = :issueStatus"),
    @NamedQuery(name = "Sheet.findByRowNumber", query = "SELECT s FROM Sheet s WHERE s.rowNumber = :rowNumber")})
public class Sheet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "DrawingNumber")
    private Integer drawingNumber;
    @Size(max = 5)
    @Column(name = "SheetNumber")
    private String sheetNumber;
    @Lob
    @Size(max = 65535)
    @Column(name = "SheetTitle")
    private String sheetTitle;
    @Size(max = 10)
    @Column(name = "OriginalIssueDate")
    private String originalIssueDate;
    @Size(max = 10)
    @Column(name = "CurrentRevisionDate")
    private String currentRevisionDate;
    @Size(max = 3)
    @Column(name = "CurrentRevisionLetter")
    private String currentRevisionLetter;
    @Column(name = "IssueStatus")
    private Character issueStatus;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "RowNumber")
    private Integer rowNumber;
   @ManyToOne
    @JoinColumn(name = "DrawingTypeID", referencedColumnName = "DrawingSizeID")
     
    private DrawingSize drawingTypeID;
    @ManyToOne
    @JoinColumn(name = "OriginalAuthorNumber", referencedColumnName = "AuthorNumber")
     
    private Author originalAuthorNumber;
    @ManyToOne
    @JoinColumn(name = "RevisionAuthorNumber", referencedColumnName = "AuthorNumber")
     
    private Author revisionAuthorNumber;

    public Sheet() {
    }

    public Sheet(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public Integer getDrawingNumber() {
        return drawingNumber;
    }

    public void setDrawingNumber(Integer drawingNumber) {
        this.drawingNumber = drawingNumber;
    }

    public String getSheetNumber() {
        return sheetNumber;
    }

    public void setSheetNumber(String sheetNumber) {
        this.sheetNumber = sheetNumber;
    }

    public String getSheetTitle() {
        return sheetTitle;
    }

    public void setSheetTitle(String sheetTitle) {
        this.sheetTitle = sheetTitle;
    }

    public String getOriginalIssueDate() {
        return originalIssueDate;
    }

    public void setOriginalIssueDate(String originalIssueDate) {
        this.originalIssueDate = originalIssueDate;
    }

    public String getCurrentRevisionDate() {
        return currentRevisionDate;
    }

    public void setCurrentRevisionDate(String currentRevisionDate) {
        this.currentRevisionDate = currentRevisionDate;
    }

    public String getCurrentRevisionLetter() {
        return currentRevisionLetter;
    }

    public void setCurrentRevisionLetter(String currentRevisionLetter) {
        this.currentRevisionLetter = currentRevisionLetter;
    }

    public Character getIssueStatus() {
        return issueStatus;
    }

    public void setIssueStatus(Character issueStatus) {
        this.issueStatus = issueStatus;
    }

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public DrawingSize getDrawingTypeID() {
        return drawingTypeID;
    }

    public void setDrawingTypeID(DrawingSize drawingTypeID) {
        this.drawingTypeID = drawingTypeID;
    }

    public Author getOriginalAuthorNumber() {
        return originalAuthorNumber;
    }

    public void setOriginalAuthorNumber(Author originalAuthorNumber) {
        this.originalAuthorNumber = originalAuthorNumber;
    }

    public Author getRevisionAuthorNumber() {
        return revisionAuthorNumber;
    }

    public void setRevisionAuthorNumber(Author revisionAuthorNumber) {
        this.revisionAuthorNumber = revisionAuthorNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rowNumber != null ? rowNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sheet)) {
            return false;
        }
        Sheet other = (Sheet) object;
        if ((this.rowNumber == null && other.rowNumber != null) || (this.rowNumber != null && !this.rowNumber.equals(other.rowNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Sheet[ rowNumber=" + rowNumber + " ]";
    }
    
}

package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.LnsDrawings;
import jpa.entities.Sheet;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-29T17:51:24")
@StaticMetamodel(DrawingSize.class)
public class DrawingSize_ { 

    public static volatile CollectionAttribute<DrawingSize, LnsDrawings> lnsDrawingsCollection;
    public static volatile SingularAttribute<DrawingSize, String> drawingSizeDimensions;
    public static volatile SingularAttribute<DrawingSize, String> drawingSizeID;
    public static volatile CollectionAttribute<DrawingSize, Sheet> sheetCollection;

}
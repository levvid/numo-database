package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.DrawingSize;
import jpa.entities.Series;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-29T17:51:24")
@StaticMetamodel(LnsDrawings.class)
public class LnsDrawings_ { 

    public static volatile SingularAttribute<LnsDrawings, Integer> drawingNumber;
    public static volatile SingularAttribute<LnsDrawings, String> cADFilePath;
    public static volatile SingularAttribute<LnsDrawings, DrawingSize> drawingSizeID;
    public static volatile SingularAttribute<LnsDrawings, Series> seriesNumber;

}
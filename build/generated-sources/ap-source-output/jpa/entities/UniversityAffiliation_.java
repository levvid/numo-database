package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-29T17:51:24")
@StaticMetamodel(UniversityAffiliation.class)
public class UniversityAffiliation_ { 

    public static volatile SingularAttribute<UniversityAffiliation, String> universityAffiliationID;
    public static volatile SingularAttribute<UniversityAffiliation, String> universityNameOrGroup;

}
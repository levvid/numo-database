package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.LnsDrawings;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-29T17:51:24")
@StaticMetamodel(Series.class)
public class Series_ { 

    public static volatile SingularAttribute<Series, String> seriesDescprition;
    public static volatile SingularAttribute<Series, LnsDrawings> lnsDrawings;
    public static volatile SingularAttribute<Series, Integer> seriesNumber;

}
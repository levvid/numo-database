package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.Sheet;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-29T17:51:24")
@StaticMetamodel(Author.class)
public class Author_ { 

    public static volatile CollectionAttribute<Author, Sheet> sheetCollection1;
    public static volatile SingularAttribute<Author, String> authorNumber;
    public static volatile SingularAttribute<Author, String> authorName;
    public static volatile CollectionAttribute<Author, Sheet> sheetCollection;
    public static volatile SingularAttribute<Author, String> employmentStatus;

}
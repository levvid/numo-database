package jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import jpa.entities.Author;
import jpa.entities.DrawingSize;
import jpa.entities.SheetPK;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-08-29T17:51:24")
@StaticMetamodel(Sheet.class)
public class Sheet_ { 

    public static volatile SingularAttribute<Sheet, Character> issueStatus;
    public static volatile SingularAttribute<Sheet, Author> revisionAuthorNumber;
    public static volatile SingularAttribute<Sheet, String> sheetTitle;
    public static volatile SingularAttribute<Sheet, String> currentRevisionDate;
    public static volatile SingularAttribute<Sheet, Author> originalAuthorNumber;
    public static volatile SingularAttribute<Sheet, SheetPK> sheetPK;
    public static volatile SingularAttribute<Sheet, String> currentRevisionLetter;
    public static volatile SingularAttribute<Sheet, String> originalIssueDate;
    public static volatile SingularAttribute<Sheet, DrawingSize> drawingTypeID;

}
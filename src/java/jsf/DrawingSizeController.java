package jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.DrawingSize;
import jpa.entities.User;
import jpa.session.DrawingSizeFacade;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;

@Named("drawingSizeController")
@SessionScoped
public class DrawingSizeController implements Serializable {

    private DrawingSize current;
    private DataModel items = null;
    @EJB
    private jpa.session.DrawingSizeFacade ejbFacade;
    private PaginationHelper pagination;
    @PersistenceContext
    private EntityManager em;
    
    private int selectedItemIndex;

    public DrawingSizeController() {
    }

    public DrawingSize getSelected() {
        if (current == null) {
            current = new DrawingSize();
            selectedItemIndex = -1;
        }
        return current;
    }

    private DrawingSizeFacade getFacade() {
        return ejbFacade;
    }
    public String backToDrawingSize(){
        recreateModel();
        getItems();
        return "List";
    } 
    /**Administrator privileges methods**/
    public boolean isAdmin(){
        User user = new User();
        boolean permission = false;
        try {
            permission =  user.isAdmin();
        } catch (IOException ex) {
            Logger.getLogger(DrawingSizeController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(DrawingSizeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return permission;
        //return true;
    }
    /*End of admin privileges methods*/
    
    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(30) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }
    public String searchDrawingSize(String drawingSize){
            //List aut = em.createNamedQuery("Author.findByAuthorName").setParameter("authorNumber",authorNumber).getResultList();
            String d = "SELECT s FROM DrawingSize s WHERE s.drawingSizeID LIKE " + "'%" + drawingSize +"%'";
            List aut = em.createQuery(d).getResultList();
            items.setWrappedData(aut);
       
            return "List";
        }
    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (DrawingSize) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new DrawingSize();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("DrawingSizeCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (DrawingSize) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("DrawingSizeUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (DrawingSize) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("DrawingSizeDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public DrawingSize getDrawingSize(java.lang.String id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = DrawingSize.class)
    public static class DrawingSizeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            DrawingSizeController controller = (DrawingSizeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "drawingSizeController");
            return controller.getDrawingSize(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof DrawingSize) {
                DrawingSize o = (DrawingSize) object;
                return getStringKey(o.getDrawingSizeID());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + DrawingSize.class.getName());
            }
        }

    }

}

package jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import jpa.entities.Author;
import jpa.entities.DrawingSize;
import jpa.entities.Sheet;
import jpa.entities.User;
import jpa.session.SheetFacade;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;

@Named("sheetController")
@SessionScoped
public class SheetController implements Serializable {

    private Sheet current;
    private List<Sheet> sortSheets = new ArrayList<Sheet>();
    private DataModel items = null;
    private boolean search = false;
    private static int first = 0;
    private int last = 0;
    private int listLength = 0;
    private boolean advancedSearch = false;
    private List sheet;
    private int itemCount = 30;
    private int lastPageItemCount = 0;
    private boolean vwAll = false;
    private boolean viewAllSearch = false;
    private boolean g = false;
    @EJB
    private jpa.session.SheetFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    @PersistenceContext
    private EntityManager em;
    private List<SelectItem> dTIDs;
    private int counter = 0;

    public SheetController() {

    }

    /*Populates drawing type ID drop down for search*/
    public List<SelectItem> getDrawingTypeIDs() {
        dTIDs = new ArrayList<SelectItem>();
        String query = "SELECT d.drawingSizeID FROM DrawingSize d";
        List drawingTypeIds;

        drawingTypeIds = em.createQuery(query).getResultList();
        dTIDs.add(new SelectItem(null, "Drawing Type ID"));
        for (int i = 0; i < drawingTypeIds.size(); i++) {
            //System.out.println("DrawingType " + i + ": " + drawingTypeIds.get(i));
            dTIDs.add(new SelectItem(drawingTypeIds.get(i), drawingTypeIds.get(i).toString()));
        }
        return dTIDs;
    }

    /**
     * Administrator privileges methods*
     */
    public boolean isAdmin() {
        User user = new User();
        boolean permission = false;
        try {
            permission = user.isAdmin();
        } catch (IOException ex) {
            Logger.getLogger(SheetController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(SheetController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return permission;
        //return true;
    }

    public boolean enableEdit() {
        return isAdmin() && !viewAllItems();
    }

    /*End of admin privileges methods*/
    /**
     * Advanced Search
     */
    public void setAdvancedSearch() {
        advancedSearch = true;
    }

    public void resetAdvancedSearch() {
        advancedSearch = false;
    }

    /*Sets up advanced search*/
    public boolean getAdvancedSearch() {
        return advancedSearch;
    }

    /*End of advanced search*/
    public String searchSheets(String sheetTitle) {

        try {
            int drawingNumber = Integer.parseInt(sheetTitle);
            String d = "SELECT s FROM Sheet s WHERE s.sheetPK.drawingNumber LIKE " + "'%" + drawingNumber + "%'";
            sheet = em.createQuery(d).getResultList();         //set the page data model
            listLength = sheet.size();
            if (listLength > 30) {
                last = 30;
            } else {
                last = listLength();
            }

            int tmpLast = 0;
            if (listLength() > 30) {
                tmpLast = 29;

            } else {
                tmpLast = listLength();
            }
            items.setWrappedData(sheet.subList(0, tmpLast));

            recreatePagination();

        } catch (NumberFormatException e) {
            String d = "SELECT s FROM Sheet s WHERE s.sheetTitle LIKE " + "'%" + sheetTitle + "%'";
            sheet = em.createQuery(d).getResultList();
            //recreatePagination();
            listLength = sheet.size();
            if (listLength > 30) {
                last = 30;
            } else {
                last = listLength();
            }
            int tmpLast = 0;
            if (listLength() > 30) {
                tmpLast = 29;

            } else {
                tmpLast = listLength();
            }
            items.setWrappedData(sheet.subList(0, tmpLast));

        }
        return "List";
    }

    // public void normalSearch(String drawingNumber, String sheetNumber, String sheetTitle, String type){
    public String searchSheets(String drawingNumber, String sheetNumber, String sheetTitle,
            String issueDate, String revisionDate, String drawingType, String originalAuthor, String revisionAuthor, boolean viewAllSearchItems) {
        String tmp = drawingNumber.replaceAll("[\\s\\u00A0]+$", ""); //check whether search string is empty, refresh if empty
        String tmp2 = sheetNumber.replaceAll("[\\s\\u00A0]+$", "");
        String tmp3 = sheetTitle.replaceAll("[\\s\\u00A0]+$", "");
        String tmp4 = issueDate.replaceAll("[\\s\\u00A0]+$", "");
        String tmp5 = revisionDate.replaceAll("[\\s\\u00A0]+$", "");
        String tmp6 = drawingType;
        String tmp7 = originalAuthor.replaceAll("[\\s\\u00A0]+$", "");
        String tmp8 = revisionAuthor.replaceAll("[\\s\\u00A0]+$", "");
        if ((tmp == "" || tmp.length() == 0) && (tmp2 == "" || tmp2.length() == 0) && (tmp3 == "" || tmp3.length() == 0)
                && (tmp4 == "" || tmp4.length() == 0) && (tmp5 == "" || tmp5.length() == 0)
                && tmp6.trim().length() < 1
                && (tmp7 == "" || tmp7.length() == 0) && (tmp8 == "" || tmp8.length() == 0) //original and revision author
                ) {
            recreateModel();
            getItems();
            resetAdvancedSearch();
            first = 0;

            search = false;
            return "List";
        }
        counter = 0;
        search = true;
//        System.out.println("Called:" + drawingNumber + "2"+sheetNumber + "2"+sheetTitle+
//                "2"+issueDate+ "2"+revisionDate+ "2"+drawingType+"2"+ originalAuthor+ "2"+revisionAuthor);
        //normalSearch(drawingNumber, sheet Number, sheetTitle, "AND");
        normalSearch(drawingNumber, sheetNumber, sheetTitle,
                issueDate, revisionDate, drawingType, originalAuthor, revisionAuthor,
                "AND", "AND", "AND", "AND", "AND", "AND", "AND", viewAllSearchItems);
        return "List";
    }

    /*from authors*/
    public String searchSheetsAdvanced(String drawingNumber, String sheetNumber, String sheetTitle,
            String issueDate, String revisionDate, String drawingType, String originalAuthor, String revisionAuthor,
            String type1, String type2, String type3, String type4, String type5, String type6, String type7, boolean viewAllSearchItems) {

        String tmp = drawingNumber.replaceAll("[\\s\\u00A0]+$", ""); //check whether search string is empty, refresh if empty
        String tmp2 = sheetNumber.replaceAll("[\\s\\u00A0]+$", "");
        String tmp3 = sheetTitle.replaceAll("[\\s\\u00A0]+$", "");
        String tmp4 = originalAuthor.replaceAll("[\\s\\u00A0]+$", "");
        String tmp5 = revisionAuthor.replaceAll("[\\s\\u00A0]+$", "");
        if ((tmp == "" || tmp.length() == 0) && (tmp2 == "" || tmp2.length() == 0) && (tmp3 == "" || tmp3.length() == 0)
                && (tmp4 == "" || tmp4.length() == 0) && (tmp5 == "" || tmp5.length() == 0)) {
            recreateModel();
            resetAdvancedSearch();
            getItems();
            first = 0;
            search = false;
            advancedSearch = false;
            return "List";
        }
        search = true;
        normalSearch(drawingNumber, sheetNumber, sheetTitle,
                issueDate, revisionDate, drawingType, originalAuthor, revisionAuthor,
                type1, type2, type3, type4, type5, type6, type7, viewAllSearchItems);
        return "List";
    }

    public void normalSearch(String drawingNumber, String sheetNumber, String sheetTitle,
            String issueDate, String revisionDate, String drawingType, String originalAuthor, String revisionAuthor,
            String type1, String type2, String type3, String type4, String type5, String type6, String type7, boolean viewAllSearchItems) {
        String search = "SELECT s FROM Sheet s WHERE ";
        if (counter == 0) {
            if (drawingNumber.trim().length() > 0) {
                search += analyseSearchString("sheetPK.drawingNumber", drawingNumber, type1) + " ";
            }
            if (sheetNumber.trim().length() > 0) {
                search += analyseSearchString("sheetPK.sheetNumber", sheetNumber, type2) + " ";
            }
            if (sheetTitle.trim().length() > 0) {
                search += analyseSearchString("sheetTitle", sheetTitle, type3) + " ";
            }
            if (issueDate.trim().length() > 0) {
                search += analyseSearchString("originalIssueDate", issueDate, type4) + " ";
            }
            if (revisionDate.trim().length() > 0) {
                search += analyseSearchString("currentRevisionDate", revisionDate, type5) + " ";
            }
            if (drawingType.trim().length() > 0) {
                search += " s.drawingTypeID.drawingSizeID =  '" + drawingType + "' " + type6;
            }

            if (originalAuthor.trim().length() > 0) {
                search += " " + analyseSearchString("originalAuthorNumber.authorNumber", originalAuthor, type7);

            }
            if (revisionAuthor.trim().length() > 0) {
                search += " " + analyseSearchString("revisionAuthorNumber.authorNumber", revisionAuthor, "");
            }
            String finalSearch = "";

            if (!search.contains("AND")) {
                finalSearch = appendAnd(search);
                search = finalSearch;
                // System.out.println("This is the search string 2: " + finalSearch);
            }
            if (search.indexOf("SELECT", 10) > 0) {
                search = search.substring(0, search.indexOf("SELECT", 10));
            }
            search = searchEndsWith(search);
            //System.out.println("FINAL SEARCH STRING: " + search);

            sheet = em.createQuery(search).getResultList();         //set the page data model
            counter++;

        }

        listLength = sheet.size();
        if (!viewAllSearchItems) {
            if (listLength > 30) {
                last = 30;
            } else {
                last = listLength();
            }
            int tmpLast = 0;
            if (listLength() > 30) {
                tmpLast = 29;
            } else {
                tmpLast = listLength();
            }
            items.setWrappedData(sheet.subList(0, tmpLast));
        } else {
            last = listLength;
            viewAllSearchItems = true;
            items.setWrappedData(sheet.subList(0, listLength));
        }

        recreatePagination();
    }

    /**
     * *end of import
     */
    public String appendAnd(String search) {
        String s = "";
        s = search.substring(0, search.indexOf("%'") + 2) + " AND " + search.substring(0, search.indexOf("%'") + 2);
        return s;
    }

    /**
     * Check whether search string ends with "AND" or "OR"
     */
    public String searchEndsWith(String search) {
        // System.out.println("This is the search string to check: " + search);
        if (search.trim().endsWith("OR")) {
            search = search.trim().substring(0, search.length() - 3);
        }
        if (search.trim().endsWith("AND")) {
            search = search.trim().substring(0, search.length() - 4);
        }

        //System.out.println("This is the resultant search string: " + search);
        return search;
    }

    /*Returns true when s is a string, false when it is a number*/
    public boolean isString(String s) {
        try {
            Integer.parseInt(s);
            return false;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    /*Handles quotes and spaces in search strings*/
    public String analyseSearchString(String field, String input, String type) {
        String search = "", parameter = " OR";
        if (input.contains("\"")) {
            input = input.replace("\"", "");
            parameter = " AND";
        }
        String[] longerSearch = input.trim().split(" ");
        if (longerSearch.length > 1 && ((input.trim().indexOf("\"") != 0) || (input.trim().indexOf("\"") != input.length() - 1))) {
            for (int i = 0; i < longerSearch.length; i++) {
                search += " s." + field + " LIKE " + "'%" + longerSearch[i] + "%'" + parameter;
                if (i == longerSearch.length - 1) {
                    search += " s." + field + " LIKE " + "'%" + longerSearch[i] + "%' " + type;
                } else {
                    search += " s." + field + " LIKE " + "'%" + longerSearch[i] + "%' " + parameter;
                }
            }
        } else {
            search += " s." + field + " LIKE " + "'%" + input + "%' " + type;
        }

        return search.trim();
    }

    /**
     * Performs searches that have OR or AND as search parameters type is the
     * type of search: one of AND or OR
     */
    public void advancedSearch(String type, String s1, String s2) {
        String first, second, search;
        //if isString then use authorName, if tis a number use authorNumber

        if (isString(s1)) {
            first = " s.sheetTitle LIKE " + "'%" + s1 + "%' " + type;
        } else {
            first = " s.sheetPK.drawingNumber LIKE " + "'%" + s1 + "%' " + type;
        }
        if (isString(s2)) {
            second = "  s.sheetTitle LIKE " + "'%" + s2 + "%'";
        } else {
            second = "  s.sheetPK.drawingNumber " + "'%" + s2 + "%'";
        }
        search = "SELECT s FROM Sheet s WHERE " + first + second;

        sheet = em.createQuery(search).getResultList();
        //set the page data model
        listLength = sheet.size();
        if (listLength > 30) {
            last = 30;
        } else {
            last = listLength();
        }
        items.setWrappedData(sheet.subList(0, last));
        recreatePagination();
    }

    /**
     * Search pagination helper methods
     */
    public int first() {
        return first;
    }

    public int last() {
        return last;
    }

    public int listLength() {
        return listLength;
    }

    public boolean searchHasPrevious() {
        return first > 29;
    }

    public boolean searchHasNext() {
        return last < listLength();
    }

    public String nextSearchItems() {
        first = first + 30;
        if (last + 30 > listLength()) {
            lastPageItemCount = listLength() - last;
            last = listLength();
        } else {
            last = last + 30;
        }
        //System.out.println("ITEM COUNT: " + listLength());
        items.setWrappedData(sheet.subList(first, last));
        return "List";
    }

    public String previousSearchItems() {
        itemCount = 30;
        first = first - 30;
        if (lastPageItemCount != 0) {
            last = last - lastPageItemCount;
            lastPageItemCount = 0;
        } else {
            last = last - 30;
        }
        items.setWrappedData(sheet.subList(first, last));

        return "List";
    }

    public boolean search() {
        return search;
    }

    /**
     * *End of Search pagination helpers*
     */
    /*Find and show all sheet items */
    public String backToSheets() {
        vwAll = true;
        List sortItems;
        String s = "SELECT s FROM Sheet s ORDER BY s.sheetPK.drawingNumber";
        sortItems = em.createQuery(s).getResultList();
        items.setWrappedData(sortItems);
        resetAdvancedSearch();
        first = 0;
        search = false;
        return "List";
    }

    public void browserBackPressed() {
        g = true;
    }

    public boolean goBack() {
        return g;
    }

    /*reset and show sheet table*/
    public String showSheetTable() {
        itemCount = 30;
        recreatePagination();
        recreateModel();
        vwAll = false;
        getPagination();
        resetAdvancedSearch();
        getItems();
        search = false;
        return "List";
    }

    public boolean viewAllItems() {
        return vwAll;
        //return true;
    }

    public boolean viewALlSearchItems() {
        return viewAllSearch;
    }

    public void resetViewAll() {
        vwAll = false;
    }

    public Sheet getSelected() {
        if (current == null) {
            current = new Sheet();
            selectedItemIndex = -1;
        }
        return current;
    }

    private SheetFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(itemCount) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Sheet) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Sheet();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("SheetCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Sheet) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("SheetUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Sheet) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("SheetDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Sheet getSheet(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Sheet.class)
    public static class SheetControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            SheetController controller = (SheetController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "sheetController");
            return controller.getSheet(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Sheet) {
                Sheet o = (Sheet) object;
                return o.getSheetPK().getDrawingNumber();
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Sheet.class.getName());
            }
        }

    }
}

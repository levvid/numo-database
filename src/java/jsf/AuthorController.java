package jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import jpa.entities.Author;
import jpa.entities.Author_;
import jpa.entities.User;
import jpa.session.AuthorFacade;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;

@Named("authorController")
@SessionScoped
public class AuthorController implements Serializable {

    private Author current;
    private DataModel items = null;

    private int search = 0;
    private static int first = 0;
    private int last = 0;
    private int listLength = 0;
    private int itemCount = 30;
    private String searchType2 = "OR";
    private int lastPageItemCount = 0;
    private HttpServletRequest request;
    List aut;
    @EJB
    private AuthorFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    @PersistenceContext
    private EntityManager em;

    public AuthorController() {
    }

    public Author getSelected() {
        if (current == null) {
            current = new Author();
            selectedItemIndex = -1;
        }
        return current;
    }

    public int search() {
        return search;
    }

    private AuthorFacade getFacade() {
        return ejbFacade;
    }

    /**
     * Administrator privileges methods*
     */
    public boolean isAdmin() {
        User user = new User();
        boolean permission = false;
        try {
            permission = user.isAdmin();
        } catch (IOException ex) {
            Logger.getLogger(AuthorController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(AuthorController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return permission;
        //return true;
    }

    /*End of admin privileges methods*/
 /*Search methods*/
    public String searchAuthors(String authorNumber, String authorName, String employmentStatus) {
        String tmp = authorName.replaceAll("[\\s\\u00A0]+$", ""); //check whether search string is empty, refresh if empty
        String tmp2 = authorNumber.replaceAll("[\\s\\u00A0]+$", ""); //check whether search string is empty, refresh if empty
        if ((tmp == "" || tmp.length() == 0) && (tmp2 == "" || tmp2.length() == 0)) {
            recreateModel();
            getItems();
            first = 0;
            search = 0;
            return "List";
        }
        search = 1;
        normalSearch(authorNumber, authorName, "AND");

        return "List";
    }

    public void normalSearch(String authorNumber, String authorName, String type) {
        String search = "SELECT a FROM Author a WHERE ";
        if (authorName.trim().length() > 0 && isString(authorName)) {
            search += analyseSearchString("authorName", authorName, type);
        }
        if (authorNumber.trim().length() > 0 && !isString(authorNumber)) {
            search += analyseSearchString("authorNumber", authorNumber, "");
        }

        if (search.trim().endsWith("OR")) {
            search = search.trim().substring(0, search.length() - 3);
        }
        if (search.trim().endsWith("AND")) {
            search = search.trim().substring(0, search.length() - 4);
        }
        if (!search.contains("AND") && authorName.trim().length() > 0 && authorNumber.trim().length() > 0) {
            search = search.substring(0, search.indexOf("a.authorNumber")) + " AND " + search.substring(search.indexOf("a.authorNumber"));
        }
        if (search.indexOf("SELECT") != 0) {
            search = search.substring(search.indexOf("SELECT", 6));
        }
        aut = em.createQuery(search).getResultList();
        //set the page data model
        listLength = aut.size();
        if (listLength > 30) {
            last = 30;
        } else {
            last = listLength();
        }

        items.setWrappedData(aut.subList(0, last));

        recreatePagination();
    }

    public String analyseSearchString(String field, String input, String type) {
        String search = "", parameter = " OR";
        if (input.contains("\"")) {
            input = input.replace("\"", "");
            parameter = " AND";
        }
        String[] longerSearch = input.trim().split(" ");
        if (longerSearch.length > 1 && ((input.trim().indexOf("\"") != 0) || (input.trim().indexOf("\"") != input.length() - 1))) {
            for (int i = 0; i < longerSearch.length; i++) {
                search += " a." + field + " LIKE " + "'%" + longerSearch[i] + "%'" + parameter;
                if (i == longerSearch.length - 1) {
                    search += " a." + field + " LIKE " + "'%" + longerSearch[i] + "%' " + type;
                } else {
                    search += " a." + field + " LIKE " + "'%" + longerSearch[i] + "%' " + parameter;
                }
            }
        } else {
            search += " a." + field + " LIKE " + "'%" + input + "%' " + type;
        }

        return search;
    }

    /*Returns true when s is a string, false when it is a number*/
    public boolean isString(String s) {
        try {
            Integer.parseInt(s);
            return false;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    /**
     * Search pagination helper methods
     */
    public int first() {
        return first;
    }

    public int last() {
        return last;
    }

    public int listLength() {
        return listLength;
    }

    public boolean searchHasPrevious() {
        return first > 29;
    }

    public boolean searchHasNext() {
        return last < listLength();
    }

    public String nextSearchItems() {
        first = first + 30;
        if (last + 30 > listLength()) {
            lastPageItemCount = listLength() - last;
            last = listLength();
        } else {
            last = last + 30;
        }
        //System.out.println("NEXT: first - " + first + " :: last - " + last);
        items.setWrappedData(aut.subList(first, last));
        return "List";
    }

    public String previousSearchItems() {
        first = first - 30;
        if (lastPageItemCount != 0) {
            last = last - lastPageItemCount;
            lastPageItemCount = 0;
        } else {
            last = last - 30;
        }
        //System.out.println("PREV: first - " + first + " :: last - " + last);
        items.setWrappedData(aut.subList(first, last));
        return "List";
    }

    /**
     * *End of pagination Search helpers*
     */
    /**
     * **********************************************************************
     */
    /*End of search methods*/
 /*Browser back button*/
    public void onload() {
        if (items.getRowCount() == 0) {
            items = null;
        }
        recreatePagination();
        recreateModel();
        getPagination();
        getItems();
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    /*Reset everything and show Author table*/
    public String backToAuthors() {
        recreatePagination();
        recreateModel();
        getItems();
        first = 0;
        search = 0;
        return "List";
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(itemCount) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }

            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Author) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Author();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("AuthorCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Author) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("AuthorUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Author) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("AuthorDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Author getAuthor(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Author.class)
    public static class AuthorControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            AuthorController controller = (AuthorController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "authorController");
            return controller.getAuthor(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Author) {
                Author o = (Author) object;
                return o.getAuthorNumber();
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Author.class.getName());
            }
        }

    }

}

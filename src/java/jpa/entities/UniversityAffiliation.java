/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "UniversityAffiliation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UniversityAffiliation.findAll", query = "SELECT u FROM UniversityAffiliation u"),
    @NamedQuery(name = "UniversityAffiliation.findByUniversityNameOrGroup", query = "SELECT u FROM UniversityAffiliation u WHERE u.universityNameOrGroup = :universityNameOrGroup"),
    @NamedQuery(name = "UniversityAffiliation.findByUniversityAffiliationID", query = "SELECT u FROM UniversityAffiliation u WHERE u.universityAffiliationID = :universityAffiliationID")})
public class UniversityAffiliation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 25)
    @Column(name = "UniversityNameOrGroup")
    private String universityNameOrGroup;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "UniversityAffiliationID")
    private String universityAffiliationID;

    public UniversityAffiliation() {
    }

    public UniversityAffiliation(String universityAffiliationID) {
        this.universityAffiliationID = universityAffiliationID;
    }

    public String getUniversityNameOrGroup() {
        return universityNameOrGroup;
    }

    public void setUniversityNameOrGroup(String universityNameOrGroup) {
        this.universityNameOrGroup = universityNameOrGroup;
    }

    public String getUniversityAffiliationID() {
        return universityAffiliationID;
    }

    public void setUniversityAffiliationID(String universityAffiliationID) {
        this.universityAffiliationID = universityAffiliationID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (universityAffiliationID != null ? universityAffiliationID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UniversityAffiliation)) {
            return false;
        }
        UniversityAffiliation other = (UniversityAffiliation) object;
        if ((this.universityAffiliationID == null && other.universityAffiliationID != null) || (this.universityAffiliationID != null && !this.universityAffiliationID.equals(other.universityAffiliationID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.UniversityAffiliation[ universityAffiliationID=" + universityAffiliationID + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "DrawingType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DrawingType.findAll", query = "SELECT d FROM DrawingType d"),
    @NamedQuery(name = "DrawingType.findByDrawingTypeID", query = "SELECT d FROM DrawingType d WHERE d.drawingTypeID = :drawingTypeID")})
public class DrawingType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Lob
    @Size(max = 65535)
    @Column(name = "Description")
    private String description;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "DrawingTypeID")
    private String drawingTypeID;

    public DrawingType() {
    }

    public DrawingType(String drawingTypeID) {
        this.drawingTypeID = drawingTypeID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDrawingTypeID() {
        return drawingTypeID;
    }

    public void setDrawingTypeID(String drawingTypeID) {
        this.drawingTypeID = drawingTypeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drawingTypeID != null ? drawingTypeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DrawingType)) {
            return false;
        }
        DrawingType other = (DrawingType) object;
        if ((this.drawingTypeID == null && other.drawingTypeID != null) || (this.drawingTypeID != null && !this.drawingTypeID.equals(other.drawingTypeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.DrawingType[ drawingTypeID=" + drawingTypeID + " ]";
    }
    
}

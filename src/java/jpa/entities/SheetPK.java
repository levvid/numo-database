/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based Sciences and Education
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author dab66
 */
@Embeddable
public class SheetPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "DrawingNumber")
    private String drawingNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "SheetNumber")
    private String sheetNumber;

    public SheetPK() {
    }

    public SheetPK(String drawingNumber, String sheetNumber) {
        this.drawingNumber = drawingNumber;
        this.sheetNumber = sheetNumber;
    }

    public String getDrawingNumber() {
        return drawingNumber;
    }

    public void setDrawingNumber(String drawingNumber) {
        this.drawingNumber = drawingNumber;
    }

    public String getSheetNumber() {
        return sheetNumber;
    }

    public void setSheetNumber(String sheetNumber) {
        this.sheetNumber = sheetNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drawingNumber != null ? drawingNumber.hashCode() : 0);
        hash += (sheetNumber != null ? sheetNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SheetPK)) {
            return false;
        }
        SheetPK other = (SheetPK) object;
        if ((this.drawingNumber == null && other.drawingNumber != null) || (this.drawingNumber != null && !this.drawingNumber.equals(other.drawingNumber))) {
            return false;
        }
        if ((this.sheetNumber == null && other.sheetNumber != null) || (this.sheetNumber != null && !this.sheetNumber.equals(other.sheetNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.SheetPK[ drawingNumber=" + drawingNumber + ", sheetNumber=" + sheetNumber + " ]";
    }
    
}

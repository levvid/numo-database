/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "Series")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Series.findAll", query = "SELECT s FROM Series s"),
    @NamedQuery(name = "Series.findBySeriesNumber", query = "SELECT s FROM Series s WHERE s.seriesNumber = :seriesNumber")})
public class Series implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "SeriesNumber")
    private Integer seriesNumber;
    @Lob
    @Size(max = 65535)
    @Column(name = "SeriesDescprition")
    private String seriesDescprition;
    @OneToOne(mappedBy = "seriesNumber")
    private LnsDrawings lnsDrawings;

    public Series() {
    }

    public Series(Integer seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

    public Integer getSeriesNumber() {
        return seriesNumber;
    }

    public void setSeriesNumber(Integer seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

    public String getSeriesDescprition() {
        return seriesDescprition;
    }

    public void setSeriesDescprition(String seriesDescprition) {
        this.seriesDescprition = seriesDescprition;
    }

    public LnsDrawings getLnsDrawings() {
        return lnsDrawings;
    }

    public void setLnsDrawings(LnsDrawings lnsDrawings) {
        this.lnsDrawings = lnsDrawings;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (seriesNumber != null ? seriesNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Series)) {
            return false;
        }
        Series other = (Series) object;
        if ((this.seriesNumber == null && other.seriesNumber != null) || (this.seriesNumber != null && !this.seriesNumber.equals(other.seriesNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Series[ seriesNumber=" + seriesNumber + " ]";
    }
    
}

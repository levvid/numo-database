/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "DrawingSize")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DrawingSize.findAll", query = "SELECT d FROM DrawingSize d"),
    @NamedQuery(name = "DrawingSize.findByDrawingSizeID", query = "SELECT d FROM DrawingSize d WHERE d.drawingSizeID = :drawingSizeID"),
    @NamedQuery(name = "DrawingSize.findByDrawingSizeDimensions", query = "SELECT d FROM DrawingSize d WHERE d.drawingSizeDimensions = :drawingSizeDimensions")})
public class DrawingSize implements Serializable {
    @OneToMany(mappedBy = "drawingTypeID")
    private Collection<Sheet> sheetCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "DrawingSizeID")
    private String drawingSizeID;
    @Size(max = 255)
    @Column(name = "DrawingSizeDimensions")
    private String drawingSizeDimensions;
    @OneToMany(mappedBy = "drawingSizeID")
    private Collection<LnsDrawings> lnsDrawingsCollection;

    public DrawingSize() {
    }

    public DrawingSize(String drawingSizeID) {
        this.drawingSizeID = drawingSizeID;
    }

    public String getDrawingSizeID() {
        return drawingSizeID;
    }

    public void setDrawingSizeID(String drawingSizeID) {
        this.drawingSizeID = drawingSizeID;
    }

    public String getDrawingSizeDimensions() {
        return drawingSizeDimensions;
    }

    public void setDrawingSizeDimensions(String drawingSizeDimensions) {
        this.drawingSizeDimensions = drawingSizeDimensions;
    }

    @XmlTransient
    public Collection<LnsDrawings> getLnsDrawingsCollection() {
        return lnsDrawingsCollection;
    }

    public void setLnsDrawingsCollection(Collection<LnsDrawings> lnsDrawingsCollection) {
        this.lnsDrawingsCollection = lnsDrawingsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drawingSizeID != null ? drawingSizeID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DrawingSize)) {
            return false;
        }
        DrawingSize other = (DrawingSize) object;
        if ((this.drawingSizeID == null && other.drawingSizeID != null) || (this.drawingSizeID != null && !this.drawingSizeID.equals(other.drawingSizeID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.DrawingSize[ drawingSizeID=" + drawingSizeID + " ]";
    }

    @XmlTransient
    public Collection<Sheet> getSheetCollection() {
        return sheetCollection;
    }

    public void setSheetCollection(Collection<Sheet> sheetCollection) {
        this.sheetCollection = sheetCollection;
    }
    
}

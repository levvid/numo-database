package jpa.entities;

import javax.servlet.http.HttpServlet;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.util.*;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Gibson Levvid
 */
public class AuthorSearch extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        HttpSession session = request.getSession(true);
        List authorList = new ArrayList();
        Connection con = null;

        String url = "jdbc:mysql://localhost:3306/";
        String db = "numo";
        String driver = "com.mysql.jdbc.Driver";
        String user = "numoadmmin";
        String pass = "n0tw4itnG4d3V1n";

        String authorNumber = "";
        authorNumber = request.getParameter("authorNumber");

        String sqlqueary = "SELECT * FROM numo WHERE AuthorNumber LIKE '%%' ";
        if (authorNumber != null && !(authorNumber.equals(""))) {
            sqlqueary += " and AuthorNumber='" + authorNumber + "'";
        }

        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url + db, user, pass);
            try {
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sqlqueary);
                while (rs.next()) {
                    List author = new ArrayList();
                    author.add(rs.getInt(1));
                    author.add(rs.getInt(2));
                    author.add(rs.getString(3));
                    author.add(rs.getString(4));
                    authorList.add(author);
                }
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("authorList", authorList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/searchAuthor.jsp");
        dispatcher.forward(request, response);
    }

}

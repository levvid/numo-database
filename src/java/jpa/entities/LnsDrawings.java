/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "LnsDrawings")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LnsDrawings.findAll", query = "SELECT l FROM LnsDrawings l"),
    @NamedQuery(name = "LnsDrawings.findByDrawingNumber", query = "SELECT l FROM LnsDrawings l WHERE l.drawingNumber = :drawingNumber"),
    @NamedQuery(name = "LnsDrawings.findByCADFilePath", query = "SELECT l FROM LnsDrawings l WHERE l.cADFilePath = :cADFilePath")})
public class LnsDrawings implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DrawingNumber")
    private Integer drawingNumber;
    @Size(max = 255)
    @Column(name = "CADFilePath")
    private String cADFilePath;
    @JoinColumn(name = "SeriesNumber", referencedColumnName = "SeriesNumber")
    @OneToOne
    private Series seriesNumber;
    @JoinColumn(name = "DrawingSizeID", referencedColumnName = "DrawingSizeID")
    @ManyToOne
    private DrawingSize drawingSizeID;

    public LnsDrawings() {
    }

    public LnsDrawings(Integer drawingNumber) {
        this.drawingNumber = drawingNumber;
    }

    public Integer getDrawingNumber() {
        return drawingNumber;
    }

    public void setDrawingNumber(Integer drawingNumber) {
        this.drawingNumber = drawingNumber;
    }

    public String getCADFilePath() {
        return cADFilePath;
    }

    public void setCADFilePath(String cADFilePath) {
        this.cADFilePath = cADFilePath;
    }

    public Series getSeriesNumber() {
        return seriesNumber;
    }

    public void setSeriesNumber(Series seriesNumber) {
        this.seriesNumber = seriesNumber;
    }

    public DrawingSize getDrawingSizeID() {
        return drawingSizeID;
    }

    public void setDrawingSizeID(DrawingSize drawingSizeID) {
        this.drawingSizeID = drawingSizeID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drawingNumber != null ? drawingNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LnsDrawings)) {
            return false;
        }
        LnsDrawings other = (LnsDrawings) object;
        if ((this.drawingNumber == null && other.drawingNumber != null) || (this.drawingNumber != null && !this.drawingNumber.equals(other.drawingNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.LnsDrawings[ drawingNumber=" + drawingNumber + " ]";
    }
    
}
